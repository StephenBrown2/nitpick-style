# Nitpick Style

A collection of nitpick styles for structuring a project's style config.
See https://nitpick.readthedocs.io for more details on configuration.

## Usage

1. Add the following to a `pyproject.toml` file in the root of your project:

    ```toml
    [tool.nitpick]
    style = "https://gitlab.com/StephenBrown2/nitpick-style/-/raw/master/nitpick-style.toml"
    ```

2. Then install nitpick

    ```sh
    pip install nitpick
    ```

3. And run flake8

    ```sh
    flake8
    ```
